/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juan
 */
@Entity
@Table(name = "paradero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paradero.findAll", query = "SELECT p FROM Paradero p")
    , @NamedQuery(name = "Paradero.findById", query = "SELECT p FROM Paradero p WHERE p.id = :id")
    , @NamedQuery(name = "Paradero.findByDireccion", query = "SELECT p FROM Paradero p WHERE p.direccion = :direccion")
    , @NamedQuery(name = "Paradero.findByUbicacion", query = "SELECT p FROM Paradero p WHERE p.ubicacion = :ubicacion")})
public class Paradero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "id")
    private String id;
    @Size(max = 60)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 80)
    @Column(name = "ubicacion")
    private String ubicacion;
    @OneToMany(mappedBy = "idultimoparadero")
    private List<Viaje> viajeList;

    public Paradero() {
    }

    public Paradero(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @XmlTransient
    public List<Viaje> getViajeList() {
        return viajeList;
    }

    public void setViajeList(List<Viaje> viajeList) {
        this.viajeList = viajeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paradero)) {
            return false;
        }
        Paradero other = (Paradero) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.transporte.entidades.Paradero[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juan
 */
@Entity
@Table(name = "buseta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Buseta.findAll", query = "SELECT b FROM Buseta b")
    , @NamedQuery(name = "Buseta.findByPlaca", query = "SELECT b FROM Buseta b WHERE b.placa = :placa")
    , @NamedQuery(name = "Buseta.findByLateral", query = "SELECT b FROM Buseta b WHERE b.lateral = :lateral")})
public class Buseta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "placa")
    private String placa;
    @Size(max = 10)
    @Column(name = "lateral")
    private String lateral;
    @JoinColumn(name = "documento_conductor", referencedColumnName = "documento")
    @ManyToOne
    private Conductor documentoConductor;
    @JoinColumn(name = "nit_empresa", referencedColumnName = "nit")
    @ManyToOne
    private Empresa nitEmpresa;
    @OneToMany(mappedBy = "placabuseta")
    private List<Viaje> viajeList;

    public Buseta() {
    }

    public Buseta(String placa) {
        this.placa = placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getLateral() {
        return lateral;
    }

    public void setLateral(String lateral) {
        this.lateral = lateral;
    }

    public Conductor getDocumentoConductor() {
        return documentoConductor;
    }

    public void setDocumentoConductor(Conductor documentoConductor) {
        this.documentoConductor = documentoConductor;
    }

    public Empresa getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(Empresa nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    @XmlTransient
    public List<Viaje> getViajeList() {
        return viajeList;
    }

    public void setViajeList(List<Viaje> viajeList) {
        this.viajeList = viajeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placa != null ? placa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Buseta)) {
            return false;
        }
        Buseta other = (Buseta) object;
        if ((this.placa == null && other.placa != null) || (this.placa != null && !this.placa.equals(other.placa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.transporte.entidades.Buseta[ placa=" + placa + " ]";
    }
    
}
